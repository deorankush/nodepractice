const inputValues = 'Please Enter Some Input Values';

function sum(input){
	var total =  0;
	if(input.length == 0)
		return inputValues;
	for(var i=0;i<input.length;i++)           
	    total += Number(input[i]);
	return 'sum of input '+ input.length +' number is = ' +total;
}

function cube(input){
	if(input.length == 0) 
		return inputValues;
	if(input.length == 1)
		return 'cude of input '+ input.length +' number is = ' +Math.pow(input,3);
	else
		return input.length + ' wrong input length, input digit should be 1 digit like [3]';
}

function division(input){
	if(input.length == 2){
       var total = input[0]/input[1];
       return total;;
	}else
		console.log('input size maximum should be equal to 2 like [8,2]');
}

function sub(input){
	var total =  input[0];
    if(input.length == 0)
        return inputValues; 
	for(var i=1;i<input.length;i++)
	    total -= Number(input[i]);
	return 'Subtraction of input '+ input.length +' number is = ' +total;
}

function mul(input){
	var total =  input[0];
    if(input.length == 0)
        return inputValues;
	for(var i=1;i<input.length;i++)                 
	    total *= Number(input[i]);
	return 'Multiple of input '+ input.length +' number is = ' +total;
}

function calc(param1,param){
	console.log(param1,param);
	switch(param1){
		case 'add': return sum(param); break;
		case 'sub': return sub(param); break;
		case 'mul': return mul(param); break;
		case 'cube': return cube(param); break;
		case 'div' : return division(param);break;
		default: return ("Error! please choose one of them(add,sub,mul,cube)");
	}
}

module.exports = { calc: calc };
